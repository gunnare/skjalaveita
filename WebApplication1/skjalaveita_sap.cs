﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SAP.Middleware.Connector; // your sap connector

namespace Applicon.Skjalaveita.SAPConnect
{
    public class ECCDestinationConfig : IDestinationConfiguration
    {

        public bool ChangeEventsSupported()
        {
            return false;
        }

        internal void GetParameters()
        {
            throw new NotImplementedException();
        }

        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public RfcConfigParameters GetParameters(string destinationName)
        {

            RfcConfigParameters parms = new RfcConfigParameters();

            if (destinationName.Equals("mySAPdestination"))
            {

                string serverSAP = ConfigurationSettings.AppSettings["SAP_SERVER"];
                string systemNumber = ConfigurationSettings.AppSettings["SYSTEM_NUMBER"];
                string clientSAP = ConfigurationSettings.AppSettings["SAP_CLIENT"];
                string userName = ConfigurationSettings.AppSettings["USER"];
                string pass = ConfigurationSettings.AppSettings["PASS"];
                string systemID = ConfigurationSettings.AppSettings["SYSTEM_ID"];

                parms.Add(RfcConfigParameters.AppServerHost, serverSAP);
                parms.Add(RfcConfigParameters.SystemNumber, systemNumber);
                parms.Add(RfcConfigParameters.SystemID, systemID);
                parms.Add(RfcConfigParameters.User, userName);
                parms.Add(RfcConfigParameters.Password, pass);
                parms.Add(RfcConfigParameters.Client, clientSAP);
                parms.Add(RfcConfigParameters.Language, "EN");
                parms.Add(RfcConfigParameters.PoolSize, "5");
            }
            return parms;

        }

        public void GetCompanies()
        {

            ECCDestinationConfig cfg = new ECCDestinationConfig();

            RfcDestinationManager.RegisterDestinationConfiguration(cfg);

            RfcDestination dest = RfcDestinationManager.GetDestination("mySAPdestination");

            RfcRepository repo = dest.Repository;

            IRfcFunction testfn = repo.CreateFunction("BAPI_COMPANYCODE_GETLIST");

            testfn.Invoke(dest);

            var companyCodeList = testfn.GetTable("COMPANYCODE_LIST");

            // companyCodeList now contains a table with companies and codes

        }
        public byte[] GetPDF(string uiid, string ssn)
        {

            ECCDestinationConfig cfg = new ECCDestinationConfig();
            if (!(RfcDestinationManager.IsDestinationConfigurationRegistered()))
            {
                RfcDestinationManager.RegisterDestinationConfiguration(cfg);
            }
            RfcDestination dest = RfcDestinationManager.GetDestination("mySAPdestination");

            RfcRepository repo = dest.Repository;
            
            IRfcFunction testfn = repo.CreateFunction("/NYH/CA_PH_GET_DOCUMENT");
           
            testfn.SetValue("I_UIID", uiid);
            testfn.SetValue("I_SSN", ssn);
            testfn.Invoke(dest);

            return testfn.GetByteArray("E_XSTRING");


        }
    }
}
 