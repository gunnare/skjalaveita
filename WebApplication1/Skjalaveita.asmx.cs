﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using Applicon.Skjalaveita.SAPConnect;


namespace Applicon.Skjalaveita
{

    [WebService(Namespace = "http://www.island.is/wsdl/postholf/v1.0/skjalaveitur")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class Skjalaveita : WebService, SkjalaveitaWEB
    {

        [WebMethod]

        public SaekjaSkjalResponse SaekjaSkjal(SaekjaSkjalRequest request)
        {
            ECCDestinationConfig ecc = new ECCDestinationConfig();
                       
            var bytestring = ecc.GetPDF(request.SkjalId, request.Kennitala);
                       
            SaekjaSkjalResponseSkjal skjal = new SaekjaSkjalResponseSkjal();
            SaekjaSkjalResponse svar = new SaekjaSkjalResponse();
            skjal.FormSkjals = "pdf";
            skjal.Innihald = bytestring;
            svar.Item = skjal;
            svar.ItemElementName = ItemChoiceType.Skjal;
            
            
            return svar;
        }

        public Task<SaekjaSkjalResponse> SaekjaSkjalAsync(SaekjaSkjalRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
